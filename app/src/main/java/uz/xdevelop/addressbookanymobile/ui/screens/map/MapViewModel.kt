package uz.xdevelop.addressbookanymobile.ui.screens.map

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import uz.xdevelop.addressbookanymobile.data.local.room.entity.PlaceModel
import uz.xdevelop.addressbookanymobile.utils.addSourceDisposable

/**
 * Created by Botirali Kozimov on 11/06/2021
 **/

class MapViewModel @ViewModelInject constructor(private val repository: MapRepository) :
    ViewModel() {

    private val _resultLiveData = MediatorLiveData<Boolean>()
    val resultLiveData: LiveData<Boolean> get() = _resultLiveData

    fun addPlaceModel(placeModel: PlaceModel) {
        _resultLiveData.addSourceDisposable(repository.insertPlace(placeModel)) { isAdded ->
            _resultLiveData.value = isAdded
        }
    }

}