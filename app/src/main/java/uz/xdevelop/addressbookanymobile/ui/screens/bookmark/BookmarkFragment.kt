package uz.xdevelop.addressbookanymobile.ui.screens.bookmark

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import uz.xdevelop.addressbookanymobile.R
import uz.xdevelop.addressbookanymobile.data.local.room.entity.PlaceModel
import uz.xdevelop.addressbookanymobile.databinding.FragmentBookmarkBinding
import uz.xdevelop.addressbookanymobile.ui.adapters.BookMarkAdapter

@AndroidEntryPoint
class BookmarkFragment : Fragment() {

    private lateinit var binding: FragmentBookmarkBinding
    private val adapter = BookMarkAdapter()

    private val viewModel: BookMarkViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentBookmarkBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        loadObservers()
        loadViews()

        viewModel.getSavePlaces()
    }

    private fun loadViews() {
        binding.list.adapter = adapter
        binding.list.layoutManager = LinearLayoutManager(requireContext())

        adapter.setOnItemClickListener { placeModel ->
            val dialog = AlertDialog.Builder(requireContext()).create()
            dialog.setButton(AlertDialog.BUTTON_NEUTRAL, "На карте") { d, v ->
                val action =
                    BookmarkFragmentDirections.actionBookmarkFragmentToMapFragment(placeModel)
                findNavController().navigate(action)
            }

            dialog.setButton(AlertDialog.BUTTON_POSITIVE, "Удалить") { d, v ->
                viewModel.deletePlaceMark(placeModel)
            }

            dialog.setOnShowListener {
                dialog.getButton(DialogInterface.BUTTON_POSITIVE)
                    .setTextColor(ContextCompat.getColor(requireContext(), R.color.buttonColor))
                dialog.getButton(DialogInterface.BUTTON_NEUTRAL)
                    .setTextColor(ContextCompat.getColor(requireContext(), R.color.buttonColor))
            }

            dialog.setTitle("Удалить этот адрес?")
            dialog.show()
        }

    }

    @SuppressLint("FragmentLiveDataObserve")
    private fun loadObservers() {
        viewModel.places.observe(this, placesObserver)
        viewModel.message.observe(this, messageObserver)
        viewModel.delete.observe(this, deleteObserver)
    }

    private val deleteObserver = Observer<Boolean> {
        showMessage("успешно удален")
    }

    private val placesObserver = Observer<List<PlaceModel>> {
        adapter.submitList(it.toMutableList())
    }

    private val messageObserver = Observer<String> {
        showMessage(it)
    }

    private fun showMessage(text: String) {
        Toast.makeText(requireContext(), text, Toast.LENGTH_SHORT).show()
    }
}