package uz.xdevelop.addressbookanymobile.ui.screens.map

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import uz.xdevelop.addressbookanymobile.data.local.room.dao.PlaceModelDao
import uz.xdevelop.addressbookanymobile.data.local.room.entity.PlaceModel
import uz.xdevelop.addressbookanymobile.utils.Coroutines
import javax.inject.Inject

/**
 * Created by Botirali Kozimov on 11/06/2021
 **/

class MapRepositoryImpl @Inject constructor(
    private val placeModelDao: PlaceModelDao
) : MapRepository {

    override fun insertPlace(placeModel: PlaceModel): LiveData<Boolean> {
        val resultLiveData = MutableLiveData<Boolean>()
        Coroutines.ioThenMain(
            { placeModelDao.insert(placeModel) },
            { id ->
                if (id != null)
                    resultLiveData.value = id > 0
            }
        )
        return resultLiveData
    }
}