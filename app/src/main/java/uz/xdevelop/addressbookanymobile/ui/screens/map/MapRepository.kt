package uz.xdevelop.addressbookanymobile.ui.screens.map

import androidx.lifecycle.LiveData
import uz.xdevelop.addressbookanymobile.data.local.room.entity.PlaceModel

/**
 * Created by Botirali Kozimov on 11/06/2021
 **/

interface MapRepository {

    fun insertPlace(placeModel: PlaceModel): LiveData<Boolean>

}