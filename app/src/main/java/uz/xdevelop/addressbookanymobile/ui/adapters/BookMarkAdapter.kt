package uz.xdevelop.addressbookanymobile.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import uz.xdevelop.addressbookanymobile.utils.SingleBlock
import uz.xdevelop.addressbookanymobile.data.local.room.entity.PlaceModel
import uz.xdevelop.addressbookanymobile.databinding.PlaceItemRecycleBinding
import uz.xdevelop.addressbookanymobile.utils.gone
import uz.xdevelop.addressbookanymobile.utils.visible

/**
 * Created by Botirali Kozimov on 11/06/2021
 **/

class BookMarkAdapter :
    ListAdapter<PlaceModel, BookMarkAdapter.ViewHolder>(DIFF_SEARCH_CALLBACK) {

    private var listenClick: SingleBlock<PlaceModel>? = null

    companion object {
        var DIFF_SEARCH_CALLBACK = object : DiffUtil.ItemCallback<PlaceModel>() {
            override fun areItemsTheSame(oldItem: PlaceModel, newItem: PlaceModel) =
                newItem.hashCode() == oldItem.hashCode()

            override fun areContentsTheSame(oldItem: PlaceModel, newItem: PlaceModel) =
                newItem.title == oldItem.title && newItem.subtitle == oldItem.subtitle && newItem.distance == oldItem.distance
        }
    }

    inner class ViewHolder(private val binding: PlaceItemRecycleBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind() {
            val placeModel = getItem(adapterPosition)
            binding.title.text = placeModel.title
            binding.subtitle.text = placeModel.subtitle
            if (placeModel.score != null) {
                binding.ratingGroup.visible()
                binding.ratings.text = placeModel.score.toString()
                binding.ratingView.rating = placeModel.score ?: 0F
                binding.reviewCount.text = placeModel.allReview.toString() + " отзыв"
            } else {
                binding.ratingGroup.gone()
            }
            binding.root.setOnClickListener { listenClick?.invoke(placeModel) }
        }
    }

    fun setOnItemClickListener(f: SingleBlock<PlaceModel>) {
        listenClick = f
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = ViewHolder(
        PlaceItemRecycleBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind()
}