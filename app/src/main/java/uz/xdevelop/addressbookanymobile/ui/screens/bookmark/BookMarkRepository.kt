package uz.xdevelop.addressbookanymobile.ui.screens.bookmark

import androidx.lifecycle.LiveData
import uz.xdevelop.addressbookanymobile.data.local.room.entity.PlaceModel
import uz.xdevelop.addressbookanymobile.utils.data.ResultData

/**
 * Created by Botirali Kozimov on 11/06/2021
 **/

interface BookMarkRepository {
    fun getAllPlaces(): LiveData<ResultData<List<PlaceModel>>>

    fun deletePlace(placeModel: PlaceModel): LiveData<ResultData<Boolean>>
}