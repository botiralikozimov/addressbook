package uz.xdevelop.addressbookanymobile.ui.screens.bookmark

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import uz.xdevelop.addressbookanymobile.utils.data.ResultData
import uz.xdevelop.addressbookanymobile.data.local.room.dao.PlaceModelDao
import uz.xdevelop.addressbookanymobile.data.local.room.entity.PlaceModel
import uz.xdevelop.addressbookanymobile.utils.Coroutines
import javax.inject.Inject

/**
 * Created by Botirali Kozimov on 11/06/2021
 **/

class BookMarkRepositoryImpl @Inject constructor(
    private val placeModelDao: PlaceModelDao
) : BookMarkRepository {

    override fun getAllPlaces(): LiveData<ResultData<List<PlaceModel>>> {
        val resultLiveData = MutableLiveData<ResultData<List<PlaceModel>>>()

        Coroutines.ioThenMain(
            { placeModelDao.getAllPlaceModels() },
            { list ->
                if (list != null)
                    if (list.isNotEmpty())
                        resultLiveData.value = ResultData.data(list)
                    else {
                        resultLiveData.value = ResultData.data(listOf())
                    }
                else
                    resultLiveData.value = ResultData.message("Ошибка")
            }
        )
        return resultLiveData
    }

    override fun deletePlace(placeModel: PlaceModel): LiveData<ResultData<Boolean>> {
        val resultLiveData = MutableLiveData<ResultData<Boolean>>()

        Coroutines.ioThenMain(
            { placeModelDao.delete(placeModel) },
            { status ->
                if (status != null)
                    if (status > 0)
                        resultLiveData.value = ResultData.data(true)
                    else
                        resultLiveData.value = ResultData.data(false)
                else
                    resultLiveData.value = ResultData.message("Ошибка")
            }
        )
        return resultLiveData
    }
}