package uz.xdevelop.addressbookanymobile.data.local.room

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import uz.xdevelop.addressbookanymobile.app.MyApp
import uz.xdevelop.addressbookanymobile.data.local.room.dao.PlaceModelDao
import uz.xdevelop.addressbookanymobile.data.local.room.entity.PlaceModel

/**
 * Created by Botirali Kozimov on 11/06/2021
 */

@Database(entities = [PlaceModel::class], version = 2)
abstract class AppDatabase : RoomDatabase() {

    abstract fun placeModelDao(): PlaceModelDao

    companion object {
        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getDatabase(): AppDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    MyApp.instance.applicationContext,
                    AppDatabase::class.java,
                    "app_database"
                )
                    .build()
                INSTANCE = instance
                return instance
            }
        }
    }

}