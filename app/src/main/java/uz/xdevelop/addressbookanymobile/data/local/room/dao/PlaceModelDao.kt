package uz.xdevelop.addressbookanymobile.data.local.room.dao

import androidx.room.Dao
import androidx.room.Query
import uz.xdevelop.addressbookanymobile.data.local.room.entity.PlaceModel

/**
 * Created by Botirali Kozimov on 11/06/2021
 **/

@Dao
interface PlaceModelDao : BaseDao<PlaceModel> {
    @Query("SELECT * FROM placemodel")
    fun getAllPlaceModels(): List<PlaceModel>
}