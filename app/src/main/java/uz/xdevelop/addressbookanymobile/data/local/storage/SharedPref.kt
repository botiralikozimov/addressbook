package uz.xdevelop.addressbookanymobile.data.local.storage

import android.content.Context
import android.content.SharedPreferences
import uz.xdevelop.addressbookanymobile.utils.DoublePreference

/**
 * Created by Botirali Kozimov on 11/06/2021
 */

class SharedPref private constructor(context: Context) {
    companion object {
        @Volatile
        lateinit var instance: SharedPref; private set

        fun init(context: Context) {
            synchronized(this) {
                instance = SharedPref(context)
            }
        }
    }

    private val pref: SharedPreferences =
        context.getSharedPreferences("SharedPref", Context.MODE_PRIVATE)

    var currentLat by DoublePreference(pref, 0.0)
    var currentLong by DoublePreference(pref, 0.0)
}