package uz.xdevelop.addressbookanymobile.data.local.room.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.yandex.mapkit.geometry.Point
import java.io.Serializable

/**
 * Created by Botirali Kozimov on 11/06/2021
 **/

@Entity
data class PlaceModel(
    @PrimaryKey(autoGenerate = true)
    var id: Int,
    var title: String,
    var subtitle: String,
    var distance: String,
    var allReview: Int?,
    var score: Float?,
    var longitude: Double,
    var latitude: Double
): Serializable