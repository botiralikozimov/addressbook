package uz.xdevelop.addressbookanymobile.utils

/**
 * Created by Botirali Kozimov on 11/06/2021
 **/

typealias SingleBlock <T> = (T) -> Unit
typealias DoubleBlock <T, E> = (T, E) -> Unit
typealias TrialBlock <T, E, F> = (T, E, F) -> Unit
typealias EmptyBlock = () -> Unit
