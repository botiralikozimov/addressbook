package uz.xdevelop.addressbookanymobile.utils

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData

/**
 * Created by Botirali Kozimov on 11/06/2021
 */

fun <T> MediatorLiveData<*>.addSourceDisposable(
    liveData: LiveData<T>,
    onChange: (T) -> Unit
): MediatorLiveData<*> {
    addSource(liveData) {
        onChange(it)
        removeSource(liveData)
    }
    return this
}

fun <T> MediatorLiveData<*>.addSourceDisposableNoAction(liveData: LiveData<T>): MediatorLiveData<*> {
    addSource(liveData) {
        removeSource(liveData)
    }
    return this
}