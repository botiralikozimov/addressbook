package uz.xdevelop.addressbookanymobile.utils

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import uz.xdevelop.addressbookanymobile.R

/**
 * Created by Botirali Kozimov on 18-Mar-21
 **/

fun Fragment.showMessage(@StringRes text: Int, block: EmptyBlock) {
    val dialog = AlertDialog.Builder(requireContext()).setMessage(text)
        .setPositiveButton("OK") { _, _ ->
            block.invoke()
        }.create()
    dialog.setOnShowListener {
        dialog.getButton(DialogInterface.BUTTON_POSITIVE)
            .setTextColor(ContextCompat.getColor(requireContext(), R.color.buttonColor))
        dialog.getButton(DialogInterface.BUTTON_NEGATIVE)
            .setTextColor(ContextCompat.getColor(requireContext(), R.color.red))
    }
    dialog.show()
}

fun Context.showMessage(@StringRes text: Int) {
    try {
        val dialog = AlertDialog.Builder(this).setTitle(text).setPositiveButton("OK", null).create()
        dialog.setOnShowListener {
            dialog.getButton(DialogInterface.BUTTON_POSITIVE)
                .setTextColor(ContextCompat.getColor(this, R.color.buttonColor))
            dialog.getButton(DialogInterface.BUTTON_NEGATIVE)
                .setTextColor(ContextCompat.getColor(this, R.color.red))
        }
        dialog.show()
    } catch (e: Exception) {
    }
}