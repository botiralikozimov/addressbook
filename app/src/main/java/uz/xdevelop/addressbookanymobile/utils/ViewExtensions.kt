package uz.xdevelop.addressbookanymobile.utils

import android.view.View

/**
 * Created by Botirali Kozimov on 11/06/2021
 **/

fun View.visible() {
    this.visibility = View.VISIBLE
}

fun View.gone() {
    this.visibility = View.GONE
}