package uz.xdevelop.addressbookanymobile.di.modules

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import uz.xdevelop.addressbookanymobile.data.local.storage.SharedPref
import javax.inject.Singleton

/**
 * Created by Botirali Kozimov on 11/06/2021
 **/

@Module
@InstallIn(ApplicationComponent::class)
class LocalStorageModule {

    @Provides
    @Singleton
    fun getLocalStorage(): SharedPref = SharedPref.instance
}