package uz.xdevelop.addressbookanymobile.di.modules

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import uz.xdevelop.addressbookanymobile.app.MyApp
import uz.xdevelop.addressbookanymobile.utils.GpsStatusLiveData
import javax.inject.Singleton

/**
 * Created by Botirali Kozimov on 11/06/2021
 **/

@Module
@InstallIn(ApplicationComponent::class)
class AppModule {

    @Provides
    @Singleton
    fun getApp(): Context = MyApp.instance

    @Provides
    @Singleton
    fun getGpsLiveData(@ApplicationContext context: Context): GpsStatusLiveData =
        GpsStatusLiveData(context)
}