package uz.xdevelop.addressbookanymobile.di.modules

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import uz.xdevelop.addressbookanymobile.ui.screens.bookmark.BookMarkRepository
import uz.xdevelop.addressbookanymobile.ui.screens.bookmark.BookMarkRepositoryImpl
import uz.xdevelop.addressbookanymobile.ui.screens.map.MapRepository
import uz.xdevelop.addressbookanymobile.ui.screens.map.MapRepositoryImpl
import javax.inject.Singleton

/**
 * Created by Botirali Kozimov on 11/06/2021
 **/

@Module
@InstallIn(ApplicationComponent::class)
interface RepositoryModule {

    @Binds
    @Singleton
    fun getMapFragmentRepository(repo: MapRepositoryImpl): MapRepository

    @Binds
    @Singleton
    fun getBookMarkRepository(repo: BookMarkRepositoryImpl): BookMarkRepository

}