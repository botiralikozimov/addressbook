package uz.xdevelop.addressbookanymobile.di.modules

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import uz.xdevelop.addressbookanymobile.data.local.room.AppDatabase
import uz.xdevelop.addressbookanymobile.data.local.room.dao.PlaceModelDao
import javax.inject.Singleton

/**
 * Created by Botirali Kozimov on 11/06/2021
 **/

@Module
@InstallIn(ApplicationComponent::class)
class DataBaseModule {

    @Provides
    @Singleton
    fun getDataBase(): AppDatabase = AppDatabase.getDatabase()

    @Provides
    @Singleton
    fun getPlaceModelDao(appDatabase: AppDatabase): PlaceModelDao = appDatabase.placeModelDao()

}