package uz.xdevelop.addressbookanymobile.app

import android.app.Application
import com.yandex.mapkit.MapKitFactory
import dagger.hilt.android.HiltAndroidApp
import uz.xdevelop.addressbookanymobile.data.local.storage.SharedPref

/**
 * Created by Botirali Kozimov on 11/06/2021
 */

@HiltAndroidApp
class MyApp : Application() {

    override fun onCreate() {
        super.onCreate()
        instance = this
        MapKitFactory.setApiKey("522fb9ba-acc3-4c2a-ad64-371448cace44")
        SharedPref.init(this)
    }

    companion object {
        lateinit var instance: MyApp
    }
}