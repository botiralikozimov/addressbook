package uz.xdevelop.addressbookanymobile

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.shape.CornerFamily
import com.google.android.material.shape.MaterialShapeDrawable
import com.yandex.mapkit.MapKitFactory
import com.yandex.mapkit.search.SearchFactory
import dagger.hilt.android.AndroidEntryPoint
import uz.xdevelop.addressbookanymobile.databinding.ActivityMainBinding
import uz.xdevelop.addressbookanymobile.utils.gone
import uz.xdevelop.addressbookanymobile.utils.visible

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
//        MapKitFactory.setLocale("ru_RU")
        MapKitFactory.initialize(this)
        SearchFactory.initialize(this)

        window.setFlags(
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        )
        loadViews()
    }

    private fun loadViews() {
        val radius = resources.getDimension(R.dimen.radius_small)
        val bottomNavigationViewBackground =
            binding.bottomMenu.background as MaterialShapeDrawable
        bottomNavigationViewBackground.shapeAppearanceModel =
            bottomNavigationViewBackground.shapeAppearanceModel.toBuilder()
                .setTopRightCorner(CornerFamily.ROUNDED, radius)
                .setTopLeftCorner(CornerFamily.ROUNDED, radius)
                .build()
        setContentView(binding.root)

        val bottomNavigationView = binding.bottomMenu
        val navController = findNavController(R.id.fragment)
        bottomNavigationView.setupWithNavController(navController)
    }

    fun showBottomMenu() {
        binding.bottomMenu.visible()
    }

    fun hideBottomMenu() {
        binding.bottomMenu.gone()
    }
}